# CS371p: Object-Oriented Programming Collatz Repo

* Name: Murray Lee

* EID: mwl647

* GitLab ID: murraylee 

* HackerRank ID: murraylee 

* Git SHA: effd80980442cfd9c62112d3fbeec54476e7e22c

* GitLab Pipelines: https://gitlab.com/murraylee/cs371p-collatz/pipelines

* Estimated completion time: 10 hours 

* Actual completion time: 12 hours

* Comments: Similar to software engineering project, except got stuck because unfamiliar with C++
