// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl, istream
#include <sstream>  // istringtstream, ostringstream

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read) {
    istringstream r("1 10\n");
    int i;
    int j;
    istream& s = collatz_read(r, i, j);
    ASSERT_TRUE(s);
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 10);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval_1) {
    const int v = collatz_eval(1, 10);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval_2) {
    const int v = collatz_eval(100, 200);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, eval_3) {
    const int v = collatz_eval(201, 210);
    ASSERT_EQ(v, 89);
}

TEST(CollatzFixture, eval_4) {
    const int v = collatz_eval(900, 1000);
    ASSERT_EQ(v, 174);
}

TEST(CollatzFixture, eval_5) {
    const int v = collatz_eval(1000, 3000);
    ASSERT_EQ(v, 217);
}

TEST(CollatzFixture, eval_6) {
    // Tests base case
    const int v = collatz_eval(1, 1);
    ASSERT_EQ(v, 1);
}

TEST(CollatzFixture, eval_7) {
    // Tests max range
    const int v = collatz_eval(1, 999999);
    ASSERT_EQ(v, 525);
}

TEST(CollatzFixture, eval_8) {
    const int v = collatz_eval(1, 3001);
    ASSERT_EQ(v, 217);
}

TEST(CollatzFixture, eval_9) {
    const int v = collatz_eval(1, 2001);
    ASSERT_EQ(v, 182);
}

TEST(CollatzFixture, eval_10) {
    // Tests when first number is greater than second
    const int v = collatz_eval(506697, 61596);
    ASSERT_EQ(v, 449);
}

TEST(CollatzFixture, eval_11) {
    const int v = collatz_eval(879198, 215976);
    ASSERT_EQ(v, 525);
}

TEST(CollatzFixture, eval_12) {
    const int v = collatz_eval(191111, 155496);
    ASSERT_EQ(v, 383);
}

TEST(CollatzFixture, eval_13) {
    const int v = collatz_eval(173940, 3043);
    ASSERT_EQ(v, 383);
}

TEST(CollatzFixture, eval_14) {
    const int v = collatz_eval(168596, 4379);
    ASSERT_EQ(v, 383);
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream w;
    collatz_print(w, 1, 10, 20);
    ASSERT_EQ(w.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream r("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n", w.str());
}
