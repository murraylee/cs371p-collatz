// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream

#include "Collatz.hpp"

using namespace std;

// ------------
// collatz_read
// ------------
istream& collatz_read (istream& r, int& i, int& j) {
    return r >> i >> j;
}


// ------------
// collatz_help
// ------------
int CACHE_SIZE = 1000000;
// lazy cache to save computed values, dynamic programming
int cache[1000000] = {0};
int collatz_help (int start, int stop)
{
    // optimize to reduce half of sequence length
    if (start < ((stop / 2)+1))
    {
        return collatz_help(stop / 2 +1, stop);
    }
    int max_cycle_length = -1;

    // loop through all values in range
    for (int num=start; num<=stop; num++)
    {
        long temp = num;

        int cycle_length = 1;
        // Computer cycle length
        while (temp > 1)
        {
            if (temp < CACHE_SIZE && cache[temp] != 0)
            {
                cycle_length += (cache[temp] - 1);
                temp = 1;
            }
            else
            {
                // collatz conjecture
                if (temp % 2 == 0)
                {
                    // bitshift to save time
                    temp = temp >> 1;
                    cycle_length++;
                }
                else
                {
                    // optimize to do (3n + 1)/2
                    temp = temp + (temp>>1) + 1;
                    cycle_length+=2;
                }
            }
        }

        // set max cycle length
        if (cycle_length > max_cycle_length)
        {
            max_cycle_length = cycle_length;
        }

        // cache all numbers in path of of cycle_length
        temp = num;
        int count = 0;
        while (temp > 1)
        {
            // already in cache
            if (temp < CACHE_SIZE && cache[temp] != 0)
            {
                temp = 1;
            }
            else if (temp < CACHE_SIZE && cache[temp] == 0)
            {
                // add to cache
                cache[temp] = cycle_length - count;

                if (temp % 2 == 0)
                {
                    temp = temp >> 1;
                    count++;
                }
                else
                {
                    temp = temp + (temp>>1) + 1;
                    count+=2;

                }
            }
            else
            {
                // collatz conjecture
                if (temp % 2 == 0)
                {
                    temp = temp >> 1;
                    count++;
                }
                else
                {
                    temp = temp + (temp>>1) + 1;
                    count+=2;
                }
            }

        }
    }
    // post conditions and type assertions
    assert(max_cycle_length != -1);
    assert(typeid(max_cycle_length) == typeid(int));
    return max_cycle_length;
}



// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j)
{
    // preconditions
    assert(i > 0 && i < 1000000);
    assert(j > 0 && j < 1000000);
    assert(typeid(i) == typeid(int));
    assert(typeid(j) == typeid(int));

    // <your code>
    // start=min, stop=max
    int start = i;
    int stop = j;
    if (start > stop)
    {
        start = j;
        stop = i;
    }

    // check to make sure start is lower end of range
    assert(start <= stop);
    return collatz_help(start, stop);
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    int i;
    int j;
    while (collatz_read(r, i, j)) {
        const int v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
